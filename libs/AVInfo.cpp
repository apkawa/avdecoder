//
// Created by apkawa on 20.12.15.
//

#include "AVInfo.h"

void AVInfo::printInfo() {
    av_log(NULL, AV_LOG_INFO, "%02d:%02d:%02d.%02d\n", hours, mins, secs, (100 * us) / AV_TIME_BASE);
    av_log(NULL, AV_LOG_INFO, "seconds: %f\n", totalSeconds);
    av_log(NULL, AV_LOG_INFO, "frames: %lu\n", totalFrames);
}

void AVInfo::parseAVFormatContext() {
    if (pFormatContext->duration != AV_NOPTS_VALUE) {
        timeBase = AV_TIME_BASE;
        if (pStream != NULL) {
//            timeBase = calculateTimeBase();
            fps = av_q2d(pStream->r_frame_rate);
        }

        u_int64_t duration = (u_int64_t) (pFormatContext->duration + 5000);
        u_long _totalSeconds = duration / timeBase;
        us = (uint) (duration % timeBase);
        mins = (uint) (_totalSeconds / 60);
        secs = (uint) (_totalSeconds % 60);
        hours = mins / 60;
        mins %= 60;
        totalSeconds = (double) _totalSeconds + (((100.0 * us) / timeBase) / 100.0);
        totalFrames = (ulong) (fps * totalSeconds);
    }

}

u_long AVInfo::calculateTimeBase() {
    return (u_long) (int64_t(pStream->codec->time_base.num) * AV_TIME_BASE) / int64_t(pStream->codec->time_base.den);
}
