//
// Created by apkawa on 20.12.15.
//

#ifndef AVDECODER_AV_INCLUDE_H
#define AVDECODER_AV_INCLUDE_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <unistd.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libswscale/swscale.h>

#include <libavfilter/avfiltergraph.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>

#include "helpers/graph2dot.h"
#include "helpers/palettegen.h"
#include "helpers/convert_frame.h"


#ifdef __cplusplus
}
#endif


#endif //AVDECODER_AV_INCLUDE_H
