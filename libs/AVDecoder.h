/*
	It is FFmpeg decoder class. Sample for article from unick-soft.ru
    https://github.com/UnickSoft/FFMpeg-decode-example
*/

#ifndef __FFMPEG_DECODER__
#define __FFMPEG_DECODER__


// http://www.ffmpeg.org/doxygen/0.9/filtering_8c.html#_details

#define min(a, b) (a > b ? b : a)

#include <string>
#include "av_include.h"

extern "C" {
#include <libavcodec/avcodec.h>
}

#include "AVInfo.h"
#include "AVPalette.h"

class AVDecoder {
    // constructor.
public:


    AVDecoder() : pImgConvertCtx(NULL), videoBaseTime(0.0),
                  videoFramePerSecond(0.0), isOpen(false), audioStreamIndex(-1), videoStreamIndex(-1),
                  pAudioCodec(NULL), pAudioCodecCtx(NULL), pVideoCodec(NULL), pVideoCodecCtx(NULL),
                  pFormatCtx(NULL), filter_spec(NULL) {

        av_register_all();
        avfilter_register_all();
    }

    // destructor.
    virtual ~AVDecoder() {
        CloseFile();
    }

    int initFilters(const char *filters_descr);

    // Open file
    virtual bool OpenFile(const char *inputFile);

    virtual bool OpenFile(const char *inputFile, const char *filter_spec) {
        this->filter_spec = (char *) filter_spec;
        OpenFile(inputFile);
    }

    // Close file and free resourses.
    virtual bool CloseFile();

    // Return next frame FFmpeg.
    virtual AVFrame *GetNextFrame();


    virtual void SeekFrame(int64_t frame_idx);

    int GetWidth() {
        return width;
    }

    int GetHeight() {
        return height;
    }

    void FindKeyFrame(int64_t frame);

    u_long getTotalFrames() {
        return videoInfo->getTotalFrames();
    }


    void printDuration() {
        if (pFormatCtx->duration != AV_NOPTS_VALUE) {
            int hours, mins, secs, us;
            int64_t duration = pFormatCtx->duration + 5000;
            int total_secs = secs = duration / AV_TIME_BASE;
            us = duration % AV_TIME_BASE;
            mins = total_secs / 60;
            secs %= 60;
            hours = mins / 60;
            mins %= 60;
            double f_total_secs = (double) total_secs + (((100.0 * us) / AV_TIME_BASE) / 100.0);
            av_log(NULL, AV_LOG_INFO, "%02d:%02d:%02d.%02d\n", hours, mins, secs, (100 * us) / AV_TIME_BASE);
            av_log(NULL, AV_LOG_INFO, "seconds: %f\n", f_total_secs);
            av_log(NULL, AV_LOG_INFO, "frames: %f\n", videoFramePerSecond * f_total_secs);
        }

    }

    // return rgb image
    AVFrame *GetRGBAFrame(AVFrame *pFrameYuv);

    AVFrame *GetRGBAFrame(AVFrame *pFrameYuv, AVRational time_base);

    void saveFrame(AVFrame *rgbFrame, char *filename);


private:
    // open video stream.
    bool OpenVideo();

    // close video stream.
    void CloseVideo();


    // Decode video buffer.
    bool DecodeVideo(const AVPacket *avpkt, AVFrame *pOutFrame);

    // FFmpeg file format.
    AVFormatContext *pFormatCtx;

    // FFmpeg codec context.
    AVCodecContext *pVideoCodecCtx;

    AVInfo *videoInfo;

    // FFmpeg codec for video.
    AVCodec *pVideoCodec;

    // FFmpeg codec context for audio.
    AVCodecContext *pAudioCodecCtx;

    // FFmpeg codec for audio.
    AVCodec *pAudioCodec;

    // Video stream number in file.
    int videoStreamIndex;

    // Audio stream number in file.
    int audioStreamIndex;

    // File is open or not.
    bool isOpen;

    // Video frame per seconds.
    double videoFramePerSecond;

    // FFmpeg timebase for video.
    int64_t videoBaseTime;


    // FFmpeg context convert image.
    struct SwsContext *pImgConvertCtx;

    // Width of image
    int width;

    // Height of image
    int height;

    bool _allowNextFrame;

    AVFrame *GetNextFrame_bak();

    bool do_filter = 0;
    AVFrame *_currentDecodedFrame;

    char *filter_spec;


public:
    AVFilterContext *buffersink_ctx;
    AVFilterContext *buffersrc_ctx;
    AVFilterGraph *filter_graph;
    int64_t last_pts = AV_NOPTS_VALUE;

    void saveFrameToPng(AVFrame *frame, char *filename);


    AVFrame *getNextDecodedFrame();

    AVFrame *getNextFilterFrame();

};

#endif