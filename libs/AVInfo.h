//
// Created by apkawa on 20.12.15.
//

#ifndef AVDECODER_AVINFO_H
#define AVDECODER_AVINFO_H


#include "av_include.h"

class AVInfo {


public:
    AVInfo(const AVFormatContext *pFormatContext, const AVStream *pStream) : pFormatContext(pFormatContext),
                                                                             pStream(pStream) {
        parseAVFormatContext();
    }

    AVInfo(const AVFormatContext *pFormatContext) : pFormatContext(pFormatContext) {
        pStream = pFormatContext->streams[0];
        parseAVFormatContext();
    }

    void printInfo();


private:
    uint hours;
    uint mins;
    uint secs;
    uint us;

    double totalSeconds;
    double fps;
public:
    ulong getTotalFrames() const {
        return totalFrames;
    }

    double getTotalSeconds() const {
        return totalSeconds;
    }

    double getFps() const {
        return fps;
    }

private:
    ulong totalFrames;

    u_long timeBase;


    const AVFormatContext *pFormatContext;
    const AVStream *pStream;

    void parseAVFormatContext();

    u_long calculateTimeBase();


};


#endif //AVDECODER_AVINFO_H
