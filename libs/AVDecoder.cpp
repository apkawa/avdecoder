/*
	It is FFmpeg decoder class. Sample for article from unick-soft.ru
*/

#include "AVDecoder.h"


bool AVDecoder::OpenFile(const char *inputFile) {
    CloseFile();


    // Open media file.
    if (avformat_open_input(&pFormatCtx, inputFile, NULL, NULL) != 0) {
        CloseFile();
        return false;
    }

    // Get format info.
    if (avformat_find_stream_info(pFormatCtx, NULL) < 0) {
        CloseFile();
        return false;
    }

    // open video and audio stream.
    bool hasVideo = OpenVideo();

    if (!hasVideo) {
        CloseFile();
        return false;
    }

    isOpen = true;

    if (videoStreamIndex != -1) {
        videoFramePerSecond = av_q2d(pFormatCtx->streams[videoStreamIndex]->r_frame_rate);
        videoBaseTime =
                (int64_t(pVideoCodecCtx->time_base.num) * AV_TIME_BASE) / int64_t(pVideoCodecCtx->time_base.den);
        videoInfo = new AVInfo(pFormatCtx, pFormatCtx->streams[videoStreamIndex]);

        //"palettegen"

        if (filter_spec != NULL && initFilters(filter_spec) < 0) {
            return false;
        }
    }
    return true;
}


bool AVDecoder::CloseFile() {
    isOpen = false;

    // Close video and audio.
    CloseVideo();

    if (pFormatCtx) {
        pFormatCtx = NULL;
    }

    return true;
}



AVFrame * AVDecoder::getNextDecodedFrame() {
    if (videoStreamIndex == -1) {
        return NULL;
    }
    AVFrame *pFrame = av_frame_alloc();
    AVPacket packet;

    int ret;
    int frame_finished;

    // Read packet.
    while (av_read_frame(pFormatCtx, &packet) >= 0) {
        if (packet.stream_index != videoStreamIndex) {
            continue;
        }

        avcodec_decode_video2(pVideoCodecCtx, pFrame, &frame_finished, &packet);
        if (!frame_finished) {
            continue;
        }

        pFrame->pts = av_frame_get_best_effort_timestamp(pFrame);
        return pFrame;

    }
    av_free_packet(&packet);
    av_frame_unref(pFrame);
    return NULL;

}


AVFrame *AVDecoder::getNextFilterFrame() {
    int ret;
    AVFrame *pFilterFrame = av_frame_alloc();
    if (_allowNextFrame) {
        _currentDecodedFrame = getNextDecodedFrame();
        // Read packet.
        if (av_buffersrc_add_frame_flags(buffersrc_ctx, _currentDecodedFrame, AV_BUFFERSRC_FLAG_KEEP_REF) < 0) {
            av_log(NULL, AV_LOG_ERROR, "Error while feeding the filtergraph\n");
        }
    }

    /* pull filtered frames from the filtergraph */
    ret = av_buffersink_get_frame(buffersink_ctx, pFilterFrame);
    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
        _allowNextFrame = 1;
        return NULL;
    }
    if (ret < 0) {
        _allowNextFrame = 1;
        return NULL;
    }
    _allowNextFrame = 0;
    return pFilterFrame;
}

AVFrame *AVDecoder::GetNextFrame() {
    if (do_filter) {
        AVFrame *filterFrame;
        while(NULL == (filterFrame = getNextFilterFrame())) {
            ;
        }
        return filterFrame;
    } else {
        return getNextDecodedFrame();
    }
    return NULL;
}

AVFrame *AVDecoder::GetNextFrame_bak() {
    if (videoStreamIndex == -1) {
        return NULL;
    }
    AVFrame *pFrame = av_frame_alloc();
    AVFrame *pFilterFrame = av_frame_alloc();
    AVPacket packet;

    int ret;
    bool do_filter = true;
    int frame_finished;

    // Read packet.
    while (av_read_frame(pFormatCtx, &packet) >= 0) {

        if (packet.stream_index != videoStreamIndex) {
            continue;
        }

        avcodec_decode_video2(pVideoCodecCtx, pFrame, &frame_finished, &packet);
        if (!frame_finished) {
            continue;
        }

        pFrame->pts = av_frame_get_best_effort_timestamp(pFrame);
        /* filter */
        if (do_filter) {
            /* push the decoded frame into the filtergraph */
            if (av_buffersrc_add_frame_flags(buffersrc_ctx, pFrame, AV_BUFFERSRC_FLAG_KEEP_REF) < 0) {
                av_log(NULL, AV_LOG_ERROR, "Error while feeding the filtergraph\n");
                break;
            }

            /* pull filtered frames from the filtergraph */
            while (1) {
                ret = av_buffersink_get_frame(buffersink_ctx, pFilterFrame);

                if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF) {
                    break;
                }
                if (ret < 0) {
                    return NULL;
                }

                return pFilterFrame;
            }
            /* end_filter */
        }
//                    av_frame_unref(frame);
        return pFrame;

    }
    av_free_packet(&packet);
    av_frame_unref(pFrame);
    av_frame_unref(pFilterFrame);

    return NULL;
}


void AVDecoder::SeekFrame(long frame_idx) {
    int64_t frame_ts = frame_idx * videoBaseTime;

    printf("seek to %i, %i, %f", frame_idx, frame_ts, videoBaseTime);
    if (av_seek_frame(pFormatCtx, videoStreamIndex, frame_idx, 0) < 0) {
        printf("fail seek %i", frame_ts);
    }
}

void AVDecoder::FindKeyFrame(int64_t frame) {
    int preceedingKeyframe = av_index_search_timestamp(
            pFormatCtx->streams[videoStreamIndex], frame,
            AVSEEK_FLAG_BACKWARD);
    printf("%i", preceedingKeyframe);
//    int64_t nearestKeyframePts = preceedingKeyframe * pFormatCtx->streams[videoStreamIndex]->time_base.den;


}


AVFrame *AVDecoder::GetRGBAFrame(AVFrame *pFrameYuv) {

    return GetRGBAFrame(pFrameYuv, buffersink_ctx->inputs[0]->time_base);

}

AVFrame *AVDecoder::GetRGBAFrame(AVFrame *pFrameYuv, AVRational time_base) {
    int64_t delay;

    if (pFrameYuv->pts != AV_NOPTS_VALUE) {
        if (last_pts != AV_NOPTS_VALUE) {
            /* sleep roughly the right amount of time;
             * usleep is in microseconds, just like AV_TIME_BASE. */
            delay = av_rescale_q(pFrameYuv->pts - last_pts,
                                 time_base, AV_TIME_BASE_Q);
            if (delay > 0 && delay < 1000000) {
                usleep(delay);
            }
        }
        last_pts = pFrameYuv->pts;
    }

    AVFrame *frame = av_frame_alloc();
    int width = pFrameYuv->width;
    int height = pFrameYuv->height;
    size_t bufferImgSize = (size_t) avpicture_get_size(PIX_FMT_BGR24, width, height);

    uint8_t *buffer = (uint8_t *) av_mallocz(bufferImgSize);

    if (frame) {
        avpicture_fill((AVPicture *) frame, buffer, PIX_FMT_RGB24, width, height);
        frame->width = width;
        frame->height = height;


        sws_scale(pImgConvertCtx,
                  pFrameYuv->data, pFrameYuv->linesize,
                  0, height, frame->data, frame->linesize);
    }

    return frame;
}


bool AVDecoder::OpenVideo() {
    bool res = false;

    if (pFormatCtx) {
        videoStreamIndex = -1;

        for (unsigned int i = 0; i < pFormatCtx->nb_streams; i++) {
            if (pFormatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
                videoStreamIndex = i;
                pVideoCodecCtx = pFormatCtx->streams[videoStreamIndex]->codec;
                av_opt_set_int(pVideoCodecCtx, "refcounted_frames", 1, 0);
                pVideoCodec = avcodec_find_decoder(pVideoCodecCtx->codec_id);

                if (pVideoCodec) {
                    res = avcodec_open2(pVideoCodecCtx, pVideoCodec, NULL) >= 0;
                    width = pVideoCodecCtx->coded_width;
                    height = pVideoCodecCtx->coded_height;
                }

                break;
            }
        }

        if (!res) {
            CloseVideo();
        }
        else {
            pImgConvertCtx = sws_getContext(pVideoCodecCtx->width, pVideoCodecCtx->height,
                                            pVideoCodecCtx->pix_fmt,
                                            pVideoCodecCtx->width, pVideoCodecCtx->height,
                                            PIX_FMT_RGB24,
                                            SWS_BICUBIC, NULL, NULL, NULL);

        }
    }

    return res;
}

bool AVDecoder::DecodeVideo(const AVPacket *avpkt, AVFrame *pOutFrame) {
    bool res = false;

    if (pVideoCodecCtx) {
        if (avpkt && pOutFrame) {
            int got_picture_ptr = 0;
            int videoFrameBytes = avcodec_decode_video2(pVideoCodecCtx, pOutFrame, &got_picture_ptr, avpkt);
            res = (videoFrameBytes > 0);
        }
    }
    return res;
}


void AVDecoder::CloseVideo() {
    if (pVideoCodecCtx) {
        avcodec_close(pVideoCodecCtx);
        pVideoCodecCtx = NULL;
        pVideoCodec = NULL;
        videoStreamIndex = 0;
    }
}


void AVDecoder::saveFrameToPng(AVFrame *frame, char *filename) {
    AVFrame * rgbFrame = av_frame_alloc();

    rgbFrame->format = PIX_FMT_RGBA;
    rgbFrame->width = frame->width;
    rgbFrame->height = frame->height;
    avpicture_alloc((AVPicture *)rgbFrame, (AVPixelFormat) rgbFrame->format, rgbFrame->width, rgbFrame->height);

    SwsContext * swCtx = sws_getContext(frame->width,
                                        frame->height,
                                        (AVPixelFormat) frame->format,
                                        rgbFrame->width,
                                        rgbFrame->height,
                                        (AVPixelFormat) rgbFrame->format,
                                        SWS_FAST_BILINEAR, 0, 0, 0);

    sws_scale(swCtx,  frame->data, frame->linesize, 0, frame->height, rgbFrame->data, rgbFrame->linesize);

    AVCodec *outCodec = avcodec_find_encoder(CODEC_ID_PNG);


    AVCodecContext *outCodecCtx = avcodec_alloc_context3(outCodec);

    outCodecCtx->width = frame->width;
    outCodecCtx->height = frame->height;

    outCodecCtx->pix_fmt = (AVPixelFormat) rgbFrame->format;
    outCodecCtx->codec_type = AVMEDIA_TYPE_VIDEO;
    outCodecCtx->time_base.num = pVideoCodecCtx->time_base.num;
    outCodecCtx->time_base.den = pVideoCodecCtx->time_base.den;

    if (!outCodec || avcodec_open2(outCodecCtx, outCodec, NULL) < 0) {
        return;
    }

    AVPacket outPacket;
    av_init_packet(&outPacket);
    outPacket.size = 0;
    outPacket.data = NULL;
    int gotFrame = 0;
    int ret = avcodec_encode_video2(outCodecCtx, &outPacket, rgbFrame, &gotFrame);
    if (ret >= 0 && gotFrame) {
        FILE *outPng = fopen(filename, "wb");
        fwrite(outPacket.data, outPacket.size, 1, outPng);
        fclose(outPng);
    }

    avcodec_close(outCodecCtx);
    av_free(outCodecCtx);

}

void AVDecoder::saveFrame(AVFrame *rgbFrame, char *filename) {
    FILE *pFile;
    int y, x;

    // Open file
    pFile = fopen(filename, "wb");
    if (pFile == NULL)
        return;

    int width = rgbFrame->width;
    int height = rgbFrame->height;
    // Write header
    fprintf(pFile, "P6\n%d %d\n255\n", width, height);

    int pixel_size = rgbFrame->linesize[0] / width;
    // Write pixel data
    for (y = 0; y < height; y++) {
        uint8_t *row = rgbFrame->data[0] + y * rgbFrame->linesize[0];
        for (x = 0; x < width; x++) {

            uint8_t *value = row + x * pixel_size;

//            if (pixel_size == 4) {
//                uint32_t n = (*(uint32_t *) value);
////                uint8_t data[4];
////                data[0] = static_cast<uint8_t >(n & 0xFF);
////                data[1] = static_cast<uint8_t >((n >> 8) & 0xFF);
////                data[2] = static_cast<uint8_t >((n >> 16) & 0xFF);
////                data[3] = static_cast<uint8_t >((n >> 24) & 0xFF);
//                n &= ~0xFF000000;
//                fwrite(&n, 2, 4, pFile);
//            } else {
//
//            }
            fwrite(value, 1, 3, pFile);

        }
    }

    // Close file
    fclose(pFile);

}

int AVDecoder::initFilters(const char *filters_descr) {
    do_filter = 1;

    char args[512];
    int ret = 0;
    AVFilter *buffersrc = avfilter_get_by_name("buffer");
    AVFilter *buffersink = avfilter_get_by_name("buffersink");
    AVFilterInOut *outputs = avfilter_inout_alloc();
    AVFilterInOut *inputs = avfilter_inout_alloc();
    AVRational time_base = pFormatCtx->streams[videoStreamIndex]->time_base;
    enum AVPixelFormat pix_fmts[] = {AV_PIX_FMT_RGB32, AV_PIX_FMT_NONE};

    filter_graph = avfilter_graph_alloc();
    if (!outputs || !inputs || !filter_graph) {
        ret = AVERROR(ENOMEM);
        goto end;
    }

    /* buffer video source: the decoded frames from the decoder will be inserted here. */
    snprintf(args, sizeof(args),
             "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
             pVideoCodecCtx->width, pVideoCodecCtx->height, pVideoCodecCtx->pix_fmt,
             time_base.num, time_base.den,

             pVideoCodecCtx->sample_aspect_ratio.num, pVideoCodecCtx->sample_aspect_ratio.den);

    ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in",
                                       args, NULL, filter_graph);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot create buffer source\n");
        goto end;
    }

    /* buffer video sink: to terminate the filter chain. */
    ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out",
                                       NULL, NULL, filter_graph);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot create buffer sink\n");
        goto end;
    }

    ret = av_opt_set_int_list(buffersink_ctx, "pix_fmts", pix_fmts,
                              AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN);
    if (ret < 0) {
        av_log(NULL, AV_LOG_ERROR, "Cannot set output pixel format\n");
        goto end;
    }

    /*
     * Set the endpoints for the filter graph. The filter_graph will
     * be linked to the graph described by filters_descr.
     */

    /*
     * The buffer source output must be connected to the input pad of
     * the first filter described by filters_descr; since the first
     * filter input label is not specified, it is set to "in" by
     * default.
     */
    outputs->name = av_strdup("in");
    outputs->filter_ctx = buffersrc_ctx;
    outputs->pad_idx = 0;
    outputs->next = NULL;

    /*
     * The buffer sink input must be connected to the output pad of
     * the last filter described by filters_descr; since the last
     * filter output label is not specified, it is set to "out" by
     * default.
     */
    inputs->name = av_strdup("out");
    inputs->filter_ctx = buffersink_ctx;
    inputs->pad_idx = 0;
    inputs->next = NULL;

    if ((ret = avfilter_graph_parse_ptr(filter_graph, filters_descr,
                                        &inputs, &outputs, NULL)) < 0)
        goto end;

    if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
        goto end;

    end:
    avfilter_inout_free(&inputs);
    avfilter_inout_free(&outputs);

//    FILE *t = fopen("/tmp/t.dot", "w");
//    graph2dot(t, filter_graph);
//    fclose(t);

    return ret;
}

