//
// Created by apkawa on 22.02.16.
//

#ifndef AVDECODER_PALETTE_H
#define AVDECODER_PALETTE_H


#include "av_include.h"
#include <palettegen.h>

class AVPalette {

public:
    AVPalette() {
        context = (PaletteGenContext) {
                .max_colors=256,
                .reserve_transparent=1,
                .stats_mode=STATS_MODE_ALL_FRAMES,
                .prev_frame=NULL
        };
    }

    void filterFrame(AVFrame *frame) {
        filter_frame(&context, frame);
    }

    AVFrame getPaletteFrame() {
        AVFrame paletteFrame = *get_palette_frame(&context);
        return paletteFrame;
    }

private:
    PaletteGenContext context;

};


#endif //AVDECODER_PALETTE_H
