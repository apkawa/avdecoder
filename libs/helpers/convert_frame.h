//
// Created by apkawa on 31.01.16.
//

#ifndef AVDECODER_CONVERT_FRAME_H
#define AVDECODER_CONVERT_FRAME_H

#ifdef __cplusplus
extern "C"
{
#endif


#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avutil.h>
#include <libswscale/swscale.h>

#include <libavfilter/avfiltergraph.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>

#include "helpers/graph2dot.h"
#include "helpers/palettegen.h"
#include "../av_include.h"

#ifdef __cplusplus
}
#endif


#ifndef __cplusplus
#define _PixelFormat enum AVPixelFormat

#endif
#ifdef __cplusplus
#define _PixelFormat AVPixelFormat
#endif



int convert_frame_colorspace(AVFrame *srcFrame, AVFrame **dstFrame, _PixelFormat format);

#endif //AVDECODER_CONVERT_FRAME_H
