//
// Created by apkawa on 31.01.16.
//

#ifndef AVDECODER_PALETTEGEN_H_H
#define AVDECODER_PALETTEGEN_H_H

#include <libavutil/avassert.h>
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include "qsort.h"

#include "avfilter.h"


/* Reference a color and how much it's used */
struct color_ref {
    uint32_t color;
    uint64_t count;
};

/* Store a range of colors */
struct range_box {
    uint32_t color;     // average color
    int64_t variance;   // overall variance of the box (how much the colors are spread)
    int start;          // index in PaletteGenContext->refs
    int len;            // number of referenced colors
    int sorted_by;      // whether range of colors is sorted by red (0), green (1) or blue (2)
};

struct hist_node {
    struct color_ref *entries;
    int nb_entries;
};

enum {
    STATS_MODE_ALL_FRAMES,
    STATS_MODE_DIFF_FRAMES,
};

#define NBITS 5
#define HIST_SIZE (1<<(3*NBITS))


typedef struct {

    int max_colors;
    int reserve_transparent;
    int stats_mode;

    AVFrame *prev_frame;                    // previous frame used for the diff stats_mode
    struct hist_node histogram[HIST_SIZE];  // histogram/hashtable of the colors
    struct color_ref **refs;                // references of all the colors used in the stream
    int nb_refs;                            // number of color references (or number of different colors)
    struct range_box boxes[256];            // define the segmentation of the colorspace (the final palette)
    int nb_boxes;                           // number of boxes (increase will segmenting them)
    int palette_pushed;                     // if the palette frame is pushed into the outlink or not
} PaletteGenContext;


int filter_frame(PaletteGenContext *s, AVFrame *in);

AVFrame * get_palette_frame(PaletteGenContext *s);

#endif //AVDECODER_PALETTEGEN_H_H
