/**
 * @file
 * Generate one palette for a whole video stream.
 */


#include "palettegen.h"


typedef int (*cmp_func)(const void *, const void *);

#define DECLARE_CMP_FUNC(name, pos)                     \
static int cmp_##name(const void *pa, const void *pb)   \
{                                                       \
    const struct color_ref * const *a = pa;             \
    const struct color_ref * const *b = pb;             \
    return   ((*a)->color >> (8 * (2 - (pos))) & 0xff)  \
           - ((*b)->color >> (8 * (2 - (pos))) & 0xff); \
}

DECLARE_CMP_FUNC(r, 0)

DECLARE_CMP_FUNC(g, 1)

DECLARE_CMP_FUNC(b, 2)

static const cmp_func cmp_funcs[] = {cmp_r, cmp_g, cmp_b};

/**
 * Simple color comparison for sorting the final palette
 */
static int cmp_color(const void *a, const void *b) {
    const struct range_box *box1 = a;
    const struct range_box *box2 = b;
    return FFDIFFSIGN(box1->color, box2->color);
}

static av_always_inline int diff(const uint32_t a, const uint32_t b) {
    const uint8_t c1[] = {a >> 16 & 0xff, a >> 8 & 0xff, a & 0xff};
    const uint8_t c2[] = {b >> 16 & 0xff, b >> 8 & 0xff, b & 0xff};
    const int dr = c1[0] - c2[0];
    const int dg = c1[1] - c2[1];
    const int db = c1[2] - c2[2];
    return dr * dr + dg * dg + db * db;
}

/**
 * Find the next box to split: pick the one with the highest variance
 */
static int get_next_box_id_to_split(PaletteGenContext *s) {
    int box_id, i, best_box_id = -1;
    int64_t max_variance = -1;

    if (s->nb_boxes == s->max_colors - s->reserve_transparent)
        return -1;

    for (box_id = 0; box_id < s->nb_boxes; box_id++) {
        struct range_box *box = &s->boxes[box_id];

        if (s->boxes[box_id].len >= 2) {

            if (box->variance == -1) {
                int64_t variance = 0;

                for (i = 0; i < box->len; i++) {
                    const struct color_ref *ref = s->refs[box->start + i];
                    variance += diff(ref->color, box->color) * ref->count;
                }
                box->variance = variance;
            }
            if (box->variance > max_variance) {
                best_box_id = box_id;
                max_variance = box->variance;
            }
        } else {
            box->variance = -1;
        }
    }
    return best_box_id;
}

/**
 * Get the 32-bit average color for the range of RGB colors enclosed in the
 * specified box. Takes into account the weight of each color.
 */
static uint32_t get_avg_color(struct color_ref *const *refs,
                              const struct range_box *box) {
    int i;
    const int n = box->len;
    uint64_t r = 0, g = 0, b = 0, div = 0;

    for (i = 0; i < n; i++) {
        const struct color_ref *ref = refs[box->start + i];
        r += (ref->color >> 16 & 0xff) * ref->count;
        g += (ref->color >> 8 & 0xff) * ref->count;
        b += (ref->color & 0xff) * ref->count;
        div += ref->count;
    }

    r = r / div;
    g = g / div;
    b = b / div;

    return 0xffU << 24 | r << 16 | g << 8 | b;
}

/**
 * Split given box in two at position n. The original box becomes the left part
 * of the split, and the new index box is the right part.
 */
static void split_box(PaletteGenContext *s, struct range_box *box, int n) {
    struct range_box *new_box = &s->boxes[s->nb_boxes++];
    new_box->start = n + 1;
    new_box->len = box->start + box->len - new_box->start;
    new_box->sorted_by = box->sorted_by;
    box->len -= new_box->len;

    av_assert0(box->len >= 1);
    av_assert0(new_box->len >= 1);

    box->color = get_avg_color(s->refs, box);
    new_box->color = get_avg_color(s->refs, new_box);
    box->variance = -1;
    new_box->variance = -1;
}

/**
 * Write the palette into the output frame.
 */
static void write_palette(PaletteGenContext *s, AVFrame *out) {
    int x, y, box_id = 0;
    uint32_t *pal = (uint32_t *) out->data[0];
    const int pal_linesize = out->linesize[0] >> 2;
    uint32_t last_color = 0;

    printf("===\n");
    for (y = 0; y < out->height; y++) {
        for (x = 0; x < out->width; x++) {
            if (box_id < s->nb_boxes) {
                pal[x] = s->boxes[box_id++].color;
                if ((x || y) && pal[x] == last_color) {
                    av_log(NULL, AV_LOG_WARNING, "Dupped color: %08X\n", pal[x]);
                }
                last_color = pal[x];
            } else {
                pal[x] = 0xff000000; // pad with black
            }
            printf("%x\n", pal[x]);
        }
        pal += pal_linesize;
    }
    printf("===\n");

    if (s->reserve_transparent) {
        av_assert0(s->nb_boxes < 256);
        pal[out->width - pal_linesize - 1] = 0x0000ff00; // add a green transparent color
    }
}


struct color_ref static **load_color_refs(const struct hist_node *hist, int nb_refs) {
    int i, j, k = 0;
    struct color_ref **refs = av_malloc_array(nb_refs, sizeof(*refs));

    if (!refs)
        return NULL;

    for (j = 0; j < HIST_SIZE; j++) {
        const struct hist_node *node = &hist[j];

        for (i = 0; i < node->nb_entries; i++)
            refs[k++] = &node->entries[i];
    }

    return refs;
}




AVFrame *get_palette_frame(PaletteGenContext *s) {
    int box_id = 0;
    struct range_box *box;

    /* reference only the used colors from histogram */
    s->refs = load_color_refs(s->histogram, s->nb_refs);
    if (!s->refs) {
        av_log(NULL, AV_LOG_ERROR, "Unable to allocate references for %d different colors\n", s->nb_refs);
        return NULL;
    }


    /* create the palette frame */
    AVFrame *out;

    enum AVPixelFormat pixel_format = PIX_FMT_RGB32;

    size_t numBytes = (size_t) avpicture_get_size(pixel_format, 16, 16);
    out = av_frame_alloc();
    uint32_t *buffer = (uint32_t *) av_mallocz(numBytes);
    avpicture_fill((AVPicture *) out, buffer, pixel_format, 16, 16);
    if (!out) {
        return NULL;
    }
    out->format = pixel_format;
    out->width = out->height = 16;
    out->pts = 0;

    /* set first box for 0..nb_refs */
    box = &s->boxes[box_id];
    box->len = s->nb_refs;
    box->sorted_by = -1;
    box->color = get_avg_color(s->refs, box);
    box->variance = -1;
    s->nb_boxes = 1;

    while (box && box->len > 1) {
        int i, rr, gr, br, longest;
        uint64_t median, box_weight = 0;

        /* compute the box weight (sum all the weights of the colors in the
         * range) and its boundings */
        uint8_t min[3] = {0xff, 0xff, 0xff};
        uint8_t max[3] = {0x00, 0x00, 0x00};
        for (i = box->start; i < box->start + box->len; i++) {
            const struct color_ref *ref = s->refs[i];
            const uint32_t rgb = ref->color;
            const uint8_t r = rgb >> 16 & 0xff, g = rgb >> 8 & 0xff, b = rgb & 0xff;
            min[0] = FFMIN(r, min[0]), max[0] = FFMAX(r, max[0]);
            min[1] = FFMIN(g, min[1]), max[1] = FFMAX(g, max[1]);
            min[2] = FFMIN(b, min[2]), max[2] = FFMAX(b, max[2]);
            box_weight += ref->count;
        }

        /* define the axis to sort by according to the widest range of colors */
        rr = max[0] - min[0];
        gr = max[1] - min[1];
        br = max[2] - min[2];
        longest = 1; // pick green by default (the color the eye is the most sensitive to)
        if (br >= rr && br >= gr) longest = 2;
        if (rr >= gr && rr >= br) longest = 0;
        if (gr >= rr && gr >= br) longest = 1; // prefer green again

        /* sort the range by its longest axis if it's not already sorted */
        if (box->sorted_by != longest) {
            cmp_func cmpf = cmp_funcs[longest];
            AV_QSORT(&s->refs[box->start], box->len, const struct color_ref *, cmpf);

            box->sorted_by = longest;
        }

        /* locate the median where to split */
        median = (box_weight + 1) >> 1;
        box_weight = 0;
        /* if you have 2 boxes, the maximum is actually #0: you must have at
         * least 1 color on each side of the split, hence the -2 */
        for (i = box->start; i < box->start + box->len - 2; i++) {
            box_weight += s->refs[i]->count;
            if (box_weight > median)
                break;
        }
        split_box(s, box, i);

        box_id = get_next_box_id_to_split(s);
        box = box_id >= 0 ? &s->boxes[box_id] : NULL;
    }


    qsort(s->boxes, s->nb_boxes, sizeof(*s->boxes), cmp_color);

    write_palette(s, out);

    return out;
}


unsigned static inline color_hash(uint32_t color) {
    const uint8_t r = color >> 16 & ((1 << NBITS) - 1);
    const uint8_t g = color >> 8 & ((1 << NBITS) - 1);
    const uint8_t b = color & ((1 << NBITS) - 1);
    return r << (NBITS * 2) | g << NBITS | b;
}


int static color_inc(struct hist_node *hist, uint32_t color) {
    int i;
    const unsigned hash = color_hash(color);
    struct hist_node *node = &hist[hash];
    struct color_ref *e;

    for (i = 0; i < node->nb_entries; i++) {
        e = &node->entries[i];
        if (e->color == color) {
            e->count++;
            return 0;
        }
    }

    e = av_dynarray2_add((void **) &node->entries, &node->nb_entries,
                         sizeof(*node->entries), NULL);
    if (!e)
        return AVERROR(ENOMEM);
    e->color = color;
    e->count = 1;
    return 1;
}


int static update_histogram_diff(struct hist_node *hist,
                                 const AVFrame *f1, const AVFrame *f2) {
    int x, y, ret, nb_diff_colors = 0;

    for (y = 0; y < f1->height; y++) {
        const uint32_t *p = (const uint32_t *) (f1->data[0] + y * f1->linesize[0]);
        const uint32_t *q = (const uint32_t *) (f2->data[0] + y * f2->linesize[0]);

        for (x = 0; x < f1->width; x++) {
            if (p[x] == q[x])
                continue;
            ret = color_inc(hist, p[x]);
            if (ret < 0)
                return ret;
            nb_diff_colors += ret;
        }
    }
    return nb_diff_colors;
}


int static update_histogram_frame(struct hist_node *hist, const AVFrame *f) {
    int x, y, ret, nb_diff_colors = 0;

    for (y = 0; y < f->height; y++) {
        const uint32_t *p = (const uint32_t *) (f->data[0] + y * f->linesize[0]);

        for (x = 0; x < f->width; x++) {
            ret = color_inc(hist, p[x]);
            if (ret < 0)
                return ret;
            nb_diff_colors += ret;
        }
    }
    return nb_diff_colors;
}


int filter_frame(PaletteGenContext *s, AVFrame *in) {
    const int ret = s->prev_frame ? update_histogram_diff(s->histogram, s->prev_frame, in)
                                  : update_histogram_frame(s->histogram, in);

    if (ret > 0)
        s->nb_refs += ret;

    if (s->stats_mode == STATS_MODE_DIFF_FRAMES) {
        s->prev_frame = in;
    }

    return ret;
}

