#include "convert_frame.h"


int convert_frame_colorspace(AVFrame *srcFrame, AVFrame **dstFrame, _PixelFormat format) {
    AVFrame *_dstFrame = *dstFrame;
    _dstFrame = av_frame_alloc();
    _dstFrame->format = format;
    _dstFrame->width = srcFrame->width;
    _dstFrame->height = srcFrame->height;
    avpicture_alloc((AVPicture *) _dstFrame, (_PixelFormat) _dstFrame->format, _dstFrame->width, _dstFrame->height);

    struct SwsContext *swCtx = sws_getContext(srcFrame->width,
                                              srcFrame->height,
                                              (_PixelFormat) srcFrame->format,
                                              _dstFrame->width,
                                              _dstFrame->height,
                                              (_PixelFormat) _dstFrame->format,

                                              SWS_FAST_BILINEAR, 0, 0, 0);

    sws_scale(swCtx, srcFrame->data, srcFrame->linesize, 0, srcFrame->height, _dstFrame->data, _dstFrame->linesize);

    return 0;
}
