#!/bin/bash
#https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu
#http://ffmpeg.zeranoe.com/builds/

PREFIX="$HOME/tmp/"
FFMPEG_SOURCE="$PREFIX/ffmpeg_source"
FFMPEG_BUILD="$PREFIX/ffmpeg_build"

#sudo apt-get update
sudo apt-get -y --force-yes install autoconf automake build-essential libass-dev libfreetype6-dev \
  libsdl1.2-dev libtheora-dev libtool libva-dev libvdpau-dev libvorbis-dev libxcb1-dev libxcb-shm0-dev \
  libxcb-xfixes0-dev pkg-config texinfo zlib1g-dev

sudo apt-get install -y yasm libx264-dev libx265-dev libmp3lame-dev libopus-dev

mkdir $FFMPEG_SOURCE -p
cd $FFMPEG_SOURCE

#wget http://ffmpeg.org/releases/ffmpeg-snapshot.tar.bz2 -o ffmpeg-snapshot.tar.bz2
#tar xjvf ffmpeg-snapshot.tar.bz2
cd ffmpeg
#rm -rf $FFMPEG_BUILD
PATH="$HOME/bin:$PATH" PKG_CONFIG_PATH="$FFMPEG_BUILD/lib/pkgconfig" ./configure \
  --prefix="$FFMPEG_BUILD" \
  --pkg-config-flags="--static" \
  --extra-cflags="-fPIC -I$FFMPEG_BUILD/include" \
  --extra-ldflags="-L$FFMPEG_BUILD/lib" \
  --bindir="$HOME/bin" \
  --enable-gpl \
  --enable-nonfree \
  --disable-libtheora \
  --enable-libx264 \
  --disable-libopus \
  \
  --disable-doc \
  --disable-network \
    --disable-iconv \
  \
  --disable-vaapi \
  --disable-d3d11va \
  --disable-dxva2  \
  --disable-vaapi \
  --disable-vda    \
  --disable-vdpau   \
  --disable-libvorbis \
  --enable-static \
#  --enable-pic \
#  --enable-shared \
#  --disable-programs \

#  --enable-shared \
#  --disable-static \

#  --enable-libfreetype \
#  --enable-libass \
#  --enable-libx265 \
#  --enable-libfdk-aac \
#  --enable-libvpx \
#  --enable-libmp3lame \
#  --enable-libtheora \
#  --enable-libvorbis \

read

PATH="$HOME/bin:$PATH" make

make install
#make distclean
hash -r