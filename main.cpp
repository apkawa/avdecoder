// ffmpegDecoder.cpp : Defines the entry point for the console application.
//

#include "libs/AVDecoder.h"


int main() {
    AVDecoder decoder;
    char filename[] = "/home/apkawa/tmp/SampleVideo_360x240_1mb.mp4";
    if (decoder.OpenFile(filename, "palettegen")) {
        int w = decoder.GetWidth();
        int h = decoder.GetHeight();
        printf("video size %ix%i\n", w, h);
        printf("total frames %i\n", decoder.getTotalFrames());

        AVPalette *palette = new AVPalette();

        uint i = 0;
        while (AVFrame *frame = decoder.GetNextFrame()) {
            palette->filterFrame(frame);
            char frame_filename[255];
            sprintf(frame_filename, "/tmp/%i.png", i);
            decoder.saveFrameToPng(frame, frame_filename);
            printf("%i frame i=%i;\n", i, frame->coded_picture_number);

            if (frame->key_frame == 1) {
                printf("is key_frame dts=%i \n", frame->pkt_dts);
            }
            i++;
        }
        AVFrame paletteFrame = palette->getPaletteFrame();
        char palettename[] = "/tmp/palette.png";
        decoder.saveFrameToPng(&paletteFrame, palettename);
        decoder.CloseFile();
    } else {
        printf("Can't open file %s\n", filename);
    }

    return 0;
}

